# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://www.funjaza.co.ke"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end

  # how it works page
  add '/howitworks', :priority => 0.7
  #sign up page
  add '/users/sign_up', :priority => 0.7
  #contact page
  add '/contact_us', :priority => 0.7

  #each event page
  Event.find_each do |event|
    add event_path(event), :lastmod => event.updated_at
  end

end
