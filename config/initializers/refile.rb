if Rails.env.development?
	Refile.cache = Refile::Backend::FileSystem.new("images/cache", max_size: 2.megabytes, hasher: Refile::RandomHasher.new)
end

if Rails.env.production?
	require "refile/s3"

	aws = {
	  region: "eu-west-1",
	  bucket: ENV['S3_BUCKET_NAME'],
		access_key_id: ENV['AWS_ACCESS_KEY_ID'],
		secret_access_key: ENV['AWS_SECRET_ACCESS_KEY']
	}

	Refile.cache = Refile::S3.new(prefix: "cache", max_size: 2.megabytes, **aws)
	Refile.store = Refile::S3.new(prefix: "store", **aws)
	# Refile.cdn_host = "https://d3lo434aeu2f08.cloudfront.net"
end