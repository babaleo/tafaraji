Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'events#index' 
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :registrations => "users/registrations" }
  resources :users, only: [:show]
  resources :contact_forms, only: [:create]
  resources :contact_organizers, only: [:create, :new]
  resources :report_issues, only: [:create, :new]
  resources :email_events, only: [:create, :new]
  resources :report_events, only: [:create, :new]
  resources :shared_events, only: [:index]
  resources :events
  resources :saved_events, only: [:create, :destroy, :index]
  get 'contact_us', to: 'contact_forms#new'
  get 'autocomplete', to: 'events#autocomplete'
  get 'howitworks' => 'static_pages#howitworks'
  get 'terms' => 'static_pages#terms'
end