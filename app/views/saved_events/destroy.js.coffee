$ ->
	# for events index page
	$("#calendar_button_<%= params[:id] %>").html('<%= j render partial: "shared/calendar_buttons", locals: { event: @dbevent } %>')

	# for saved_events index page
	$(".row-<%= params[:id] %>").remove()

	if $('.result-row').length == 0 && $('#saved-events-results').length > 0 && (savedEventsPage == "1" || savedEventsPage == "") && $(".pagination").length == 0
		$('#no-saved-events-message').show()
	else if $('.result-row').length == 0 && $('#saved-events-results').length > 0 && (savedEventsPage == "1" || savedEventsPage == "") && $(".pagination").length > 0
		$('#redirect-saved-events-page1').show()
		window.location.href = "/saved_events"
	else if $('.result-row').length == 0 && $('#saved-events-results').length > 0 && (savedEventsPage != "1" && savedEventsPage != "")
		$('#redirect-saved-events-page1').show()
		window.location.href = "/saved_events"
	
	return