$('#event-results-wrapper').html('<%= j render "event_results" %>')
$('#results-summary-wrapper').html('<%= j render "results_summary", events: @events %>')
$("#aggs-wrapper").html('<%= j render "aggGroups" %>')
$("#nearby-wrapper").html('<%= j render "nearby_events" %>')
$("#search-wrapper").html('<%= j render "search" %>')
$("#upcoming-ongoing-tabs").html('<%= j render "upcoming_ongoing_tabs" %>')
autoComplete() # this ensures that the autocomplete method is bound to the searchbox even after it is reloaded through AJAX
moreTimesAvailable() # this ensures that the moreTimesAvailable method is bound to the more times link even after it is reloaded through AJAX
applyToolTip() # this ensures that a tooltip is displayed on mouseover edit link