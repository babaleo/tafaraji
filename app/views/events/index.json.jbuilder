json.array!(@events) do |event|
  json.extract! event, :id, :title, :description, :starts, :ends, :user_id, :event_category_id, :schedule, :recurs_until, :location, :place_id, :entry, :point
  json.url event_url(event, format: :json)
end
