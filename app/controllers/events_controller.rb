class EventsController < ApplicationController
  after_filter :store_location, only: [:show]
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show, :index, :autocomplete]
  before_action :correct_user, only: [:edit, :update, :destroy]
  before_action :set_cache_buster, only: [:show, :index]
  impressionist :actions => [:show], :unique => [:session_hash]
  

  # GET /events
  # GET /events.json
  def index

    # enable this if lots of events are added in many counties
    # hmm doesnt work with clear filters
    # params[:l] ||= current_user.location.name if user_signed_in?

    options = {
      event_category_name: params[:e],
      from: params[:f],
      to: params[:t],
      date: params[:d],
      entry: params[:n],
      location: params[:l],
      lat: params[:lat],
      lng: params[:lng],
      km: params[:km],
      upcoming_ongoing: params[:uo]
    }

    ongoing_options = {
      event_category_name: params[:e],
      from: params[:f],
      to: params[:t],
      date: params[:d],
      entry: params[:n],
      location: params[:l],
      lat: params[:lat],
      lng: params[:lng],
      km: params[:km],
      upcoming_ongoing: "ongoing"
    }

    @events = Event.search(params[:q], options).page(params[:page]).per_page(10)

    # if params[:uo] == "ongoing"
    #   event_responses = @events.results.response.each

    #   @total_ongoing_events = 0

    #   event_responses.each do |event_response|
    #     ongoing_event_time = event_response.event_times.select { |event_time| event_time.starts < Time.zone.now && event_time.ends > Time.zone.now }
    #     if ongoing_event_time.count > 0
    #       @total_ongoing_events += 1
    #     end
    #   end
    # else
    #   @ongoing_events = Event.search(params[:q], ongoing_options)

    #   event_responses = @ongoing_events.results.response.each

    #   @total_ongoing_events = 0

    #   event_responses.each do |event_response|
    #     ongoing_event_time = event_response.event_times.select { |event_time| event_time.starts < Time.zone.now && event_time.ends > Time.zone.now }
    #     if ongoing_event_time.count > 0
    #       @total_ongoing_events += 1
    #     end
    #   end
    # end

    if params[:uo] == "ongoing"
      # event_responses = @events.results.response.each

      @total_ongoing_events = @events.count

      # event_responses.each do |event_response|
      #   ongoing_event_time = event_response.event_times.select { |event_time| event_time.starts < Time.zone.now && event_time.ends > Time.zone.now }
      #   if ongoing_event_time.count > 0
      #     @total_ongoing_events += 1
      #   end
      # end
    else
      @ongoing_events = Event.search(params[:q], ongoing_options)
      @total_ongoing_events = @ongoing_events.count

    end

    # passing a specific set of predetermined params to aggregation links to avoid Cross Site Scripting vulnerability
    @clean_params = { 
      q: params[:q], 
      e: params[:e], 
      f: params[:f], 
      t: params[:t], 
      d: params[:d], 
      n: params[:n], 
      l: params[:l], 
      lat: params[:lat], 
      lng: params[:lng], 
      km: params[:km], 
      uo: params[:uo] 
    }

    set_meta_tags og: {
      title: "FunJaza - Find & Share Fun Events",
      description: "Find fun events happening across Kenya. And if you're planning one, share it for free!",
      image: "#{root_url}images/funjaza_logo_and_pacifico_tag_1200_1200.jpg"
    }
    set_meta_tags twitter: {
      card: "summary_large_image",
      title: "FunJaza - Find & Share Fun Events",
      description: "Find fun events happening across Kenya. And if you're planning one, share it for free!",
      image: "#{root_url}images/funjaza_logo_and_pacifico_tag_1200_1200.jpg"
    }

    respond_to do |format|
      format.html
      format.js
    end

  end

  # GET /events/1
  # GET /events/1.json
  def show
    impressionist(@event)
    set_meta_tags og: {
      title: @event.title,
      description: @event.descriptiontext
    }
    set_meta_tags twitter: {
      card: "summary_large_image",
      title: @event.title,
      description: @event.descriptiontext
    }
  end

  # GET /events/new
  def new
    @event = Event.new
    # @event_new = true
    @event.event_schedules.build unless @event.event_schedules.any?
    @event.tickets.build unless @event.tickets.any?
  end

  # GET /events/1/edit
  def edit
    # @event.event_times.build unless @event.event_times.any?
    @event.event_schedules.build unless @event.event_schedules.any?
    @event.tickets.build unless @event.tickets.any?
  end

  # POST /events
  # POST /events.json
  def create

    # begin
    @event = current_user.events.build(event_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    # begin
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Event was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def autocomplete
    options = {
      event_category_name: params[:e],
      from: params[:f],
      to: params[:t],
      date: params[:d],
      entry: params[:n],
      location: params[:l],
      lat: params[:lat],
      lng: params[:lng],
      km: params[:km],
      upcoming_ongoing: params[:uo]
    } 

    render json: Event.autocomplete(params[:term], options).map { |e| 
      [
        e.slug,
        e.title,
        (e.event_category.name).titleize,
        e.venue,
        if e.event_times.count == 1 
          e.event_times[0].starts_string
        else
          "#{e.event_times[0].starts_string} + more times"
        end,
        e.try(:highlight).try('descriptiontext.autocomplete').try(:join)
      ]
    }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:title, 
                                    :description, 
                                    :user_id, 
                                    :event_category_id, 
                                    :venue,
                                    :location_id,
                                    :entry, 
                                    :point,
                                    :poster,
                                    :remove_poster,
                                    :confirm_truth,
                                    # event_times_attributes: [:id, :starts_date, :starts_time, :ends_date, :ends_time, :repeats, :_destroy],
                                    event_schedules_attributes: [:id, :starts_date, :starts_time, :ends_date, :ends_time, :schedule, :until, :_destroy],
                                    tickets_attributes: [:id, :name, :price, :_destroy])
    end

    def correct_user
      @event = Event.friendly.find(params[:id])
      redirect_to root_url unless @event.user == current_user
    end

    def set_cache_buster
      response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
      response.headers["Pragma"] = "no-cache"
      response.headers["Expires"] = "Fri, 05 Oct 1984 00:00:00 GMT"
    end

    def store_location
      # store last url - this is needed for post-login redirect to whatever the user last visited.
      return unless request.get? 
      if (request.path != "/users/sign_in" &&
          request.path != "/users/sign_up" &&
          request.path != "/users/password/new" &&
          request.path != "/users/password/edit" &&
          request.path != "/users/confirmation" &&
          request.path != "/users/sign_out" &&
          !request.xhr?) # don't store ajax calls
        session[:previous_url] = request.fullpath 
      end
    end

end
