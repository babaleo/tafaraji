class ReportIssuesController < ApplicationController
  def new
  	@report_issue = ReportIssue.new
  end

  def create
  	begin 
  		@report_issue = ReportIssue.new(params[:report_issue]) 
  		@report_issue.request = request
  		if @report_issue.deliver
  			flash.now[:notice] = 'Message successfully sent.'
  		else
  			render :new 
  		end 
  	rescue ScriptError
      respond_to do |format|
        flash.now[:error] = 'Sorry, this message appears to be spam and was not delivered.'
        format.html { render :new }
      end
  	end
  end
end