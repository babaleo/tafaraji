class ContactFormsController < ApplicationController
  def new
  	@contact_form = ContactForm.new
  end

  def create
  	begin 
  		@contact_form = ContactForm.new(params[:contact_form]) 
  		@contact_form.request = request
  		if @contact_form.deliver
  			flash.now[:notice] = 'Message has been sent.'
  		else
  			render :new 
  		end 
  	rescue ScriptError
      respond_to do |format|
        flash.now[:error] = 'Sorry, this message appears to be spam and was not delivered.'
        format.html { render :new }
      end
  	end
  end
end