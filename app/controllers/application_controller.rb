class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  # this takes care of chrome caching ajax responses and returning js code in the page after a back action
  before_action :set_cache_buster_for_ajax

  # allows for additional params to be added to devise views and be submitted
  before_action :configure_permitted_parameters, if: :devise_controller?

  # calls prepare_meta_tags before every get request
  before_action :prepare_meta_tags, if: "request.get?"

  protected

	  def configure_permitted_parameters
	    devise_parameter_sanitizer.for(:sign_up) << :name
	    devise_parameter_sanitizer.for(:account_update) << :name
	    devise_parameter_sanitizer.for(:sign_up) << :location_id
	    devise_parameter_sanitizer.for(:account_update) << :location_id
	    # devise_parameter_sanitizer.for(:sign_up) << :twitter_handle
	    # devise_parameter_sanitizer.for(:account_update) << :twitter_handle
	    # devise_parameter_sanitizer.for(:sign_up) << :facebook_url
	    # devise_parameter_sanitizer.for(:account_update) << :facebook_url
	  end

	  def prepare_meta_tags(options={})

	    site_name   = "FunJaza | Find Fun. Share Fun."
	    title       = [controller_name, action_name].join(" ")
	    description = "Easily find and share information about upcoming fun events in Kenya."
	    current_url = request.url

	    # Let's prepare a nice set of defaults

	    defaults = {
	      site:        site_name,
	      title:       title,
	      description: description,  
	      keywords:    %w[funjaza fun entertainment event kenya nairobi],
	      twitter:     {site_name: site_name,
	                    site: '@FunJaza',
	                    card: 'summary',
	                    description: description
	                   },
	      og:          {url: current_url,
	                    site_name: site_name,
	                    title: title,
	                    description: description,
	                    type: 'website'}
	    }


	    options.reverse_merge!(defaults)

	    set_meta_tags options

	  end

 	private

	  def set_cache_buster_for_ajax
			if request.xhr?
		  	response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
		  	response.headers["Pragma"] = "no-cache"
		  	response.headers["Expires"] = "Fri, 05 Oct 1984 00:00:00 GMT"
		  end
	  end

end
