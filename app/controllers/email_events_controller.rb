class EmailEventsController < ApplicationController

  def new
    @email_event = EmailEvent.new
    @event_to_be_emailed = Event.friendly.find(params[:id])
    @subject = @event_to_be_emailed.title
    @message = "Hi,\r\n\r\nI found the following event on FunJaza that might interest you.\r\n\r\n#{@event_to_be_emailed.title} - #{event_url(@event_to_be_emailed)}"
  end
  
  def create
  	begin 
  		@email_event = EmailEvent.new(params[:email_event])

      respond_to do |format|
        if @email_event.deliver
          format.js {}
          format.json { render json: @email_event, status: :created, location: @email_event }
        else
          format.json { render json: { error: @email_event.errors.full_messages}, status: :unprocessable_entity }
        end
      end
  	rescue ScriptError
      respond_to do |format|
        format.json { render json: { error: @email_event.errors.full_messages}, status: :unprocessable_entity }
      end
  	end
  end

end