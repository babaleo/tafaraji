require 'will_paginate/array'

class SavedEventsController < ApplicationController
	before_action :authenticate_user!
  before_action :set_cache_buster, only: [:index]
	
	def create
		begin
			@saved_event = current_user.saved_events.build(saved_event_params)
			if @saved_event.save
				@dbevent = Event.find_by(id: params[:event_id])
				respond_to do |format|
					format.html { redirect_to events_path, notice: 'Event has been successfully saved.' }
					format.js
				end
			end
		rescue ActiveRecord::RecordNotUnique
			respond_to do |format|
				format.html { redirect_to events_path, error: 'Event already saved in the past.' }
			end
		end
	end

	def destroy
		current_user.saved_events.where(event_id: params[:id]).first.try(:destroy)
		@dbevent = Event.find_by(id: params[:id])
		respond_to do |format|
			format.html { redirect_to :back, notice: 'Event successfully removed from saved collection.'}
			format.js
		end
	end

	def index
		saved_events_event_ids = current_user.saved_events.map { |saved_event| saved_event.event.id }
		unordered_ongoing_upcoming_saved_events = Event.joins(:event_times).where(id: saved_events_event_ids).where("event_times.ends > ?", Time.zone.now).references(:event_times).distinct
		upcoming_events_ordered = EventTime.where(event_id: saved_events_event_ids).where("starts > ?", Time.zone.now).order(:starts).pluck(:event_id).uniq
		ongoing_events_ordered = EventTime.where(event_id: saved_events_event_ids).where("starts < ?", Time.zone.now).where("ends > ?", Time.zone.now).order(:starts).pluck(:event_id).uniq
		upcoming_ongoing_ordered_events = upcoming_events_ordered.unshift(ongoing_events_ordered).flatten.uniq
		@ongoing_upcoming_saved_events = unordered_ongoing_upcoming_saved_events.sort_by { |e| upcoming_ongoing_ordered_events.index(e.id) || upcoming_ongoing_ordered_events.length }
		@ongoing_upcoming_saved_events = @ongoing_upcoming_saved_events.paginate(:page => params[:page], :per_page => 10)
	end

	private

		def saved_event_params
      params.permit(:event_id)
    end

    def set_cache_buster
      response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
      response.headers["Pragma"] = "no-cache"
      response.headers["Expires"] = "Fri, 05 Oct 1984 00:00:00 GMT"
    end

end