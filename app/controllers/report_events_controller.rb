class ReportEventsController < ApplicationController

  def new
    @report_event = ReportEvent.new
    @event_to_be_emailed = Event.friendly.find(params[:id])
    @subject = "Report Event: #{@event_to_be_emailed.title}"
  end
  
  def create
  	begin 
  		@report_event = ReportEvent.new(params[:report_event])

      respond_to do |format|
        if @report_event.deliver
          format.js {}
          format.json { render json: @report_event, status: :created, location: @report_event }
        else
          format.json { render json: { error: @report_event.errors.full_messages}, status: :unprocessable_entity }
        end
      end
  	rescue ScriptError
      respond_to do |format|
        format.json { render json: { error: @report_event.errors.full_messages}, status: :unprocessable_entity }
      end
  	end
  end

end