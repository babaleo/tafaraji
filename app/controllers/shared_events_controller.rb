class SharedEventsController < ApplicationController
	before_action :authenticate_user!

	def index
		if params[:search]
      @shared_events = Event.shared_search(params[:search]).where(user_id: current_user.id).order(created_at: :desc).page(params[:page]).per_page(15)
    else
      @shared_events = Event.where(user_id: current_user.id).order(created_at: :desc).page(params[:page]).per_page(15)
    end
	end
end