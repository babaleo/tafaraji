# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# this takes care of chrome caching ajax responses and returning js code in the page after a back action
$ ->
	$.ajaxSetup cache: false
	return

$ ->
	$("#event-title").on 'propertychange input textInput', ->
		left = 80 - $(this).val().length
		if left < 0
			$("#event-title-counter").addClass "overlimit"
		else
			$("#event-title-counter").removeClass "overlimit"
		$("#event-title-counter").text "Remaining: " + left
		return
	return

$ ->
  $("#event-title").on "focus", ->
    $("#event-title-counter").show()
    return
  return

$ ->
	$("#event-title").on "blur", ->
		$("#event-title-counter").hide()
		return
	return

$ ->
  $(".datetime .date").datepicker
      format: "dd/mm/yyyy"
      todayHighlight: true
      clearBtn: true
      disableTouchKeyboard: true
    .keydown ->
      $(this).datepicker "hide"
      return
    .on 'changeDate', (ev) -> #added to take care of ClientSideValidation
      $(ev.target).focus()
      $(ev.target).blur()
      return

    $(".datetime .time").timepicker
      showDuration: true
      timeFormat: "g:ia"

    # initialize datepair
    $(".datetime").datepair("defaultTimeDelta": null)

  $('#event-times')
  .on 'cocoon:before-insert', (e, added_event_time) ->
    added_event_time.fadeIn('slow');
    return
  .on 'cocoon:after-insert', (e, added_event_time) ->
    $(".datetime .date").datepicker
      format: "dd/mm/yyyy"
      todayHighlight: true
      clearBtn: true
      disableTouchKeyboard: true
    .keydown ->
      $(this).datepicker "hide"
      return
    $(".datetime .time").timepicker
      showDuration: true
      timeFormat: "g:ia"

    # initialize datepair
    $(".datetime").datepair("defaultTimeDelta": null)
    return
  .on 'cocoon:before-remove', (e, removed_event_time) ->
    $(this).data('remove-timeout', 500)
    removed_event_time.fadeOut('slow')
    return
  return

$ ->
  $(".until-date").datepicker
      format: "dd/mm/yyyy"
      todayHighlight: true
      clearBtn: true
      disableTouchKeyboard: true
    .keydown ->
      $(this).datepicker "hide"
      return
    .on 'changeDate', (ev) -> #added to take care of ClientSideValidation
      $(ev.target).focus()
      $(ev.target).blur()
      return

  $('#event-times')
  .on 'cocoon:after-insert', (e, added_event_time) ->
    $(".until-date").datepicker
      format: "dd/mm/yyyy"
      todayHighlight: true
      clearBtn: true
      disableTouchKeyboard: true
    .keydown ->
      $(this).datepicker "hide"
      return
    return
  return

$ ->
  $('input:file').change ->

    # console.log "got you"
    #checks if browser support the File API in HTML5
    if window.File and window.FileReader and window.FileList and window.Blob
      #get the file size and file type from file input field
      fsize = $('.file-upload')[0].files[0].size
      ftype = $('.file-upload')[0].files[0].type

      # console.log ftype
      # check if file is larger than 1MB and reset file input field if found to be larger than 1MB
      if fsize > 2097152
        alert "Wololo! The poster you selected is too large! It must be less than 2MB."
        $('.file-upload').val('')

      # check if file is png, gif or jpeg and reset file input field if found to be of another type
      switch ftype
        when 'image/png', 'image/gif', 'image/jpeg'
        else
          alert "Woi! The file type you selected is not supported. Supported file types are PNG, JPEG and GIF."
          $('.file-upload').val('')
    return
  return

# hides last nested row to be removed in case validation causes the page to reload
# using a regex selector woohoo
$ ->
  if $("[id^=event_event_times_attributes_][id$=__destroy]").val() is "true"
    nested_fields = $("[id^=event_event_times_attributes_][id$=__destroy]").parent().parent()
    nested_fields.hide()
  return

$(document).on 'upload:start', 'form', (e) ->
  $(this).find('input[type=submit]').attr 'disabled', true
  $('.poster-uploading').show()
  return

$(document).on 'upload:complete', 'form', (e) ->
  if !$(this).find('input.uploading').length
    $(this).find('input[type=submit]').removeAttr 'disabled'
    $('.poster-uploading').hide()
  return

$ ->
  $('#event-description').summernote(
    height: 200
    minHeight: null
    maxHeight: null
    toolbar: [
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['strikethrough', 'superscript', 'subscript']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['insert', ['link']],
      ['misc', ['fullscreen', 'undo', 'redo']]
    ]
    )
  return

# ensures that paste is always plain text
$ ->
  $('#event-description').on 'summernote.paste', (customEvent, e) ->
    bufferText = ((e.originalEvent or e).clipboardData or window.clipboardData).getData('Text')
    e.preventDefault()
    setTimeout (->
      document.execCommand 'insertText', false, bufferText
      return
    ), 10
    return
  return

# when user changes value
$ ->
  $('select[name="event[entry]"]').on "change", ->
    if @value is "Paid"
      $("#tickets").show("slow")
    else
      $("#tickets").hide("slow")
    return
  return

# when new/edit event page loads
$ ->
  if $('#event-entry').val() is "Paid"
    $("#tickets").show()
  else
    $("#tickets").hide()
  return

$ ->
  $("#event-venue").geocomplete
    details: "form"
    detailsAttribute: "data_geo"
    types: [ 'geocode','establishment']
    map: "#map-preview"
    mapOptions:
      draggable: false
    # country: I18n.t "country_code" #the i18n-js gem may be able to allow multiple countries
    country: 'ke'
    autoselect: false
    maxZoom: 17
  .bind "geocode:result", (event, result) ->
    # console.log result
    $(".map-container").show()
    map = $(this).geocomplete("map")
    marker = $(this).geocomplete("marker")
    google.maps.event.addListenerOnce map, "idle", ->
      google.maps.event.trigger map, "resize"
      map.setCenter(marker.getPosition())
      if map.getZoom() < 12
        map.setZoom(12)
      return
    return
  return

$ ->
  $("#event-venue").on "focus", ->
    if $(this).val().length > 0 && $(".hidden-map-info").val().length > 0
      $(this).val("")
      $(".hidden-map-info").val("")
      $(".map-container").hide("slow")
      $(".event-admin-area").val("")
    return
  return

initialize = (lat, lng) ->
  mapPreview = document.getElementById('map-preview')
  pointLatLng = new (google.maps.LatLng)(lat, lng)
  mapOptions = 
    center: pointLatLng
    zoom: 17
    mapTypeId: google.maps.MapTypeId.ROADMAP
  map = new (google.maps.Map)(mapPreview, mapOptions)
  marker = new (google.maps.Marker)(
  	position: pointLatLng
  	map: map)
  return

# See: https://github.com/DavyJonesLocker/client_side_validations-simple_form/issues/38
$ ->
  ClientSideValidations.formBuilders['SimpleForm::FormBuilder'].wrappers.vertical_form =
    add: (element, settings, message) ->
      errorElement = element.parent().find "#{settings.error_tag}.#{settings.error_class}"
      if not errorElement[0]?
        wrapper_tag_element = element.closest(settings.wrapper_tag)
        errorElement = $("<#{settings.error_tag}/>", { class: settings.error_class, text: message })
        wrapper_tag_element.append(errorElement)
      wrapper_class_element = element.closest(".#{settings.wrapper_class}");
      wrapper_class_element.addClass(settings.wrapper_error_class)
      errorElement.text(message)
    remove: (element, settings) ->
      wrapper_class_element = element.closest(".#{settings.wrapper_class}.#{settings.wrapper_error_class}")
      wrapper_tag_element = element.closest(settings.wrapper_tag)
      wrapper_class_element.removeClass(settings.wrapper_error_class)
      errorElement = wrapper_tag_element.find("#{settings.error_tag}.#{settings.error_class}")
      errorElement.remove()
  return

window.ClientSideValidations.callbacks.form.fail = () ->
  alert "Please correct all errors on the form before submitting it."
  return

window.moreTimesAvailable = () -> 
  $('.more-times-available-link').click ->
    $(this).text (i, old) ->
      if old == 'show more times'
        $(this).text('hide more times')
        $(this).css("color","#D35400")
      else
        $(this).text('show more times')
        $(this).css("color","#337ab7")
      return
    return
  return

$ ->
  moreTimesAvailable()
  return

$ ->
  $('#show-hide-map-link').click ->
    $(this).text (i, old) ->
      if old == 'show map'
        $(this).text('hide map')
        $(this).css("color","#D35400")
      else 
        $(this).text('show map')
        $(this).css("color","#337ab7")
      return
    return
  return

# events index page history pushstate - specific to filters
$ ->
  $("#aggs-wrapper").on 'click', '.agg-list-item a[data-remote=true]', (e) ->
    url = $(this).attr('href')
    history.pushState {}, '', url
    return
  return

# events index page history pushstate - specific to clear links for filters, search and nearby lookups
$ ->
  $("#results-summary-wrapper").on 'click', 'a[data-remote=true].clear-filters-and-search', (e) ->
    url = $(this).attr('href')
    history.pushState {}, '', url
    return
  return

# events index page history pushstate - specific to links in upcoming ongoing tabs
$ ->
  $("#upcoming-ongoing-tabs").on 'click', 'a[data-remote=true].upcoming-ongoing-link', (e) ->
    url = $(this).attr('href')
    history.pushState {}, '', url
    return
  return

# events index page history popstate - allows back and forward actions
$ ->
  $(window).on 'popstate', (e) ->
    $.getScript location.href
    # the if statement below takes care of browsers that fire popstate on first page load
    # if e.originalEvent.state != null
    #   console.log e
    #   $.getScript location.href
    return
  return

$ ->
  $('.new_event, .edit_event').areYouSure();
  return