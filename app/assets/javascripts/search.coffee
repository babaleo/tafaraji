# this function cleans up <em> tags in highlighted description fragments
stripHTML = (dirtyString) ->
	container = document.createElement('div')
	container.innerHTML = dirtyString
	container.textContent or container.innerText


window.autoComplete = -> # using window ensures the function is accessible globally
	if $("#searchbox-input-group").length > 0 #the code below will only run on the page where div#searchbox-input-group is defined
		$('#searchbox').autocomplete(
			source: (request, response) ->
				$.ajax
					url: '/autocomplete.json'
					dataType: 'json'
					data: { 
									term: request.term, 
									e: $(".hidden_event_category").val(),
									f: $(".hidden_from").val(),
									t: $(".hidden_to").val(),
									d: $(".hidden_date").val(),
									n: $(".hidden_entry").val(),
									l: $(".hidden_location").val(),
									km: $(".hidden_km").val(),
									lat: $(".hidden_lat").val(),
									lng: $(".hidden_lng").val(),
									uo: $(".hidden_uo").val()
								}
					success: (data) ->
						objects = $.map(data, (v) ->
							if v[5] is null
								labelText = v[1] + " - " + v[2] + " - " + v[3] + " - " + v[4]
							else
								descriptionFragment = stripHTML(v[5])
								labelText = v[1] + " - " + v[2] + " - " + v[3] + " - " + v[4] + " - Details: ..." + descriptionFragment + "..."
							label: labelText
							value: v[1]
							event_slug: v[0]
						)
						response objects
			open: ->
				searchboxWidth = $('#searchbox').css('width')
				$('.ui-autocomplete').css('width', searchboxWidth)
			select: (event,ui) ->
				window.location.href = "/events/" + ui.item.event_slug
		).data('ui-autocomplete')._renderItem = (ul, item) ->
			keywords = @term.split(" ").join('|')
			newText = String(item.label).replace(new RegExp(keywords, "gi"),"<span class='highlight'>$&</span>")
			$('<li></li>').data('ui-autocomplete-item', item).append('<a>' + newText + '</a>').appendTo ul

$ ->
	autoComplete()
	return