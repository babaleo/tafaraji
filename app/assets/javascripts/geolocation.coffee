# checks if geolocation is supported on browser
# can using a cookie make this one call per session?
$ ->
	if $("#find-nearby-events-wrapper").length > 0 #the code below will only run on the page where div#searchbox-input-group is defined
	  if !(navigator.geolocation) 
	    $(".find-nearby-events-btn").css('display', 'none')
	    $("#search-or-nearby-label").css('display', 'none')
	return

$ ->
	$("#nearby-wrapper").on "click", ".find-nearby-events-btn", (e) ->
	  startPos = undefined
	  
	  geoOptions = ->
	    timeout: 10 * 1000 #10 secs
	    maximumAge: 60 * 1000 #60 secs
	    return

	  geoSuccess = (position) ->
	    startPos = position
	    # set returned coordinates and search radius then submit form
	    $("#hidden-lat").val(startPos.coords.latitude)
	    $("#hidden-lng").val(startPos.coords.longitude)
	    $("#hidden-search-radius").val("5km")
	    # submit form with coordinates and radius in km
	    $("#find-nearby-events-form").submit()
	    return

	  geoError = (error) ->
	    $(".fa-spin-nearby").hide()
	    switch error.code
	      when error.TIMEOUT
	        alert "Woi! The request for your location timed out. Please try again."
	      when error.PERMISSION_DENIED
	        # The user has disabled location services
	        alert "To help you find nearby events, this website needs permission to access to your browser's location service."
	      when error.POSITION_UNAVAILABLE
	        # unable to get coordinates e.g. perhaps the client's internet connection is unavailable
	        alert "Woi! We we're unable to get your current location. Please try again. If this issue persists, check that your internet connection is active."
	    return

	  $(".fa-spin-nearby").show()
	  $(".fa-location-arrow-nearby").hide()
	  navigator.geolocation.getCurrentPosition geoSuccess, geoError, geoOptions
	  return
	return

