# disable nprogress spinner that fires when turbolinks is in effect
# NProgress.configure
#   showSpinner: false

# start: alternates collapsible areas and the active button
$ ->
  $(".navbar-collapse").on "shown.bs.collapse", ->
    # if $("#navbar-collapse-search").hasClass("collapsing")
    # 	$("#navbar-collapse-search").collapse("hide")

    $("button.navbar-toggle").addClass("activebtn")
    return
  return

# $ ->
#   $("#navbar-collapse-search").on "shown.bs.collapse", ->
#     if $("#navbar-collapse").hasClass("collapsing")
#     	$("#navbar-collapse").collapse("hide")

#     # sets focus on search box
#     input = $("#xs-searchbox")
#     temp = input.val()
#     input.focus().val('').blur().focus().val(temp)

#     $("button#navbar-toggle-search").addClass("activebtn")
#     return
#   return
# end: alternates collapsible areas and the active button

# $ ->
#   $(document).ajaxStart ->
#     console.log 'Triggered ajaxStart handler.'
#     return
#   return

# start: takes focus away from buttons when their respective collapse areas are hidden
# $ ->
#   $("#navbar-collapse-search").on "hidden.bs.collapse", ->
#     $("#navbar-toggle-search").blur()

#     $("button#navbar-toggle-search").removeClass("activebtn")
#     return
#   return

$ ->
  $(".navbar-collapse").on "hidden.bs.collapse", ->
    $(".navbar-toggle").blur()
    $("button.navbar-toggle").removeClass("activebtn")
    return
  return
# end: takes focus away from buttons when their respective collapse areas are hidden

# # hides search form after query is submitted
# $ ->
#   $("#navbar-search-form-xs").submit ->
#     if $("#navbar-collapse-search").hasClass("in")
#       $("#navbar-collapse-search").collapse("hide")
#     return
#   return

# # The two event handlers below close collapsible search and menu when user clicks away from them
# $(document).on 'click', (e) ->
#   if $("#navbar-collapse-search").hasClass("in") && !$(e.target).is("form#navbar-search-form-xs,input#xs-searchbox")
#     $("#navbar-collapse-search").collapse("hide")
#   return

$(document).on 'click', (e) ->
  if $(".navbar-collapse").hasClass("in")
    $(".navbar-collapse").collapse("hide")
  return

$ ->
  if !('ontouchstart' of document.documentElement)
    el = document.documentElement
    if !($(el).hasClass('no-touch'))
      document.documentElement.className += ' no-touch'
  return

# takes care of fixed navbar when page anchor is used on a page
shiftWindow = ->
  scrollBy 0, -53
  return

if location.hash
  shiftWindow()
window.addEventListener 'hashchange', shiftWindow


# $ ->
#   if $("#adBottom").length > 0
#     $("footer").addClass("footer-adBottom")
#   return

window.applyToolTip = () ->
  $('[data-toggle="tooltip"]').tooltip()
  return

$ ->
  applyToolTip()
  return

# $ ->
#   $('.call-to-signup-msg').typed
#     strings: [
#       "CONCERTS"
#       'PLAYS'
#       'FILMS'
#       'PARTIES'
#       'NIGHTLIFE'
#       'SPORTS'
#       'ART'
#       'FASHION'
#       "SHARE FUN^1000, FOR FREE! "
#     ]
#     typeSpeed: 0
#     showCursor: false
#     startDelay: 1000
#     cursorChar: "_"
#   return

# $ ->
#   $('.call-to-signup-msg').typed
#     strings: [
#       "CONCERTS"
#       'PLAYS'
#       'FILMS'
#       'PARTIES'
#       'NIGHTLIFE'
#       'SPORTS'
#       'ART'
#       'FASHION'
#       'SHARE FUN^1000, FOR FREE! &nbsp; <a href="/users/sign_up" class="btn btn-sign-up">Sign up</a>'
#     ]
#     typeSpeed: 0
#     showCursor: false
#     startDelay: 1500
#     cursorChar: "_"
#   return