module ApplicationHelper
	# returns the full title on a per-page basis.
	# def full_title(page_title = '')
	# 	base_title = "FunJaza"
	# 	if page_title.empty?
	# 		base_title
	# 	else
	# 		"#{base_title} | #{page_title}"
	# 	end
	# end

	# helps to set styling for notices and alerts by defining the relevant class
	def bootstrap_class_for flash_type
    case flash_type
      when "success"
        "alert-success"
      when "error"
        "alert-danger"
      when "alert"
        "alert-warning"
      when "notice"
        "alert-info"
      else
        flash_type.to_s
    end
  end
end