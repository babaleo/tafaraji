class ContactOrganizer < MailForm::Base

  attribute :name, :validate => true
  attribute :your_email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :subject, :validate => true
  attribute :message, :validate => true
  attribute :nickname, :captcha  => true
  attribute :uid, :validate => true

  # Declare the e-mail headers. It accepts anything the mail method in ActionMailer accepts.
  def headers
    {
      :subject => "#{subject}",
      :to => "#{User.friendly.find(uid).email}",
      :from => %("#{name}" <#{your_email}>)
    }
  end
end