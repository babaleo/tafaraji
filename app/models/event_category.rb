class EventCategory < ActiveRecord::Base
  has_many :events
	# this triggers reindexing of documents in ElasticSearch when event types are modified e.g. renamed
  after_update { self.events.each(&:touch) }
  
  validates :name, presence: true
end
