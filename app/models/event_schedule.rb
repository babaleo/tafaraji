require 'chronic'

class EventSchedule < ActiveRecord::Base
  include IceCube
  serialize :schedule, Hash
  belongs_to :event
  has_many :event_times, dependent: :destroy

  after_create :generate_event_times
  before_update :modify_event_times
  before_destroy :destroy_event_times
  before_validation :set_to_end_of_day

	Chronic::Parser::DEFAULT_OPTIONS[:endian_precedence] = [:little, :middle]
  Chronic.time_class = Time.zone
	DateTimeAttribute.parser = Chronic
	date_time_attribute :starts, :ends

  validates :starts_date, presence: true
  validates :starts_time, presence: true
  validates :starts_date, date: true
  validates :starts_time, date: true
  validates :ends_date, presence: true
  validates :ends_time, presence: true
  validates :starts, uniqueness: { scope: :event_id,  message: "must be unique i.e. each event's start date/time must be unique" }
  validates :until, presence: true, :if => :schedule?
  validate :until_date_must_be_greater_than_start
  validates :ends_date, date: { after: :starts_date, before: :twenty_four_hours_later, message: 'must be less than 24 hours from event\'s start' }
  validates :ends_time, date: { after: :starts_time, before: :twenty_four_hours_later, message: 'must be less than 24 hours from event\'s start' }

  def schedule=(new_schedule)
    if RecurringSelect.is_valid_rule?(new_schedule)
      write_attribute(:schedule, RecurringSelect.dirty_hash_to_rule(new_schedule).to_hash)
    else
      write_attribute(:schedule, nil)
    end
  end
  
  def converted_schedule
    the_schedule = Schedule.new(self.starts, end_time: self.ends)
    the_schedule.add_recurrence_rule(RecurringSelect.dirty_hash_to_rule(self.schedule))
    the_schedule
  end

  private
    def twenty_four_hours_later
      self.starts.to_datetime + 24.hours
    end

    def generate_event_times
      if self.schedule.empty?
        event_time = self.event_times.build(starts: self.starts, ends: self.ends, event_id: self.event_id)
        event_time.save
      else
        self.converted_schedule.occurrences(self.until).each do |occurrence|
          event_time = self.event_times.build(starts: occurrence.start_time, ends: occurrence.end_time, event_id: self.event_id)
          event_time.save
        end
      end
    end

    def modify_event_times
      if self.schedule.empty?
        self.event_times.destroy_all
        event_time = self.event_times.build(starts: self.starts, ends: self.ends, event_id: self.event_id)
        event_time.save
      else
        self.event_times.destroy_all
        self.converted_schedule.occurrences(self.until).each do |occurrence|
          event_time = self.event_times.build(starts: occurrence.start_time, ends: occurrence.end_time, event_id: self.event_id)
          event_time.save
        end
      end
    end

    def destroy_event_times
      self.event_times.destroy_all
    end

    def set_to_end_of_day
      if self.until != nil
        self.until = self.until.end_of_day
      end
    end

    def until_date_must_be_greater_than_start
      errors.add(:until, "must be set to a date later than the start date") if
        !self.until.blank? and self.until.to_date <= self.starts.to_date
    end

end