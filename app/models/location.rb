class Location < ActiveRecord::Base
	has_many :events
	has_many :users
	# this triggers reindexing of documents in ElasticSearch when event types are modified e.g. renamed
  after_update { self.events.each(&:touch) }
  
	validates_uniqueness_of :name, message: "Location by the same name found. Location name must be unique."
end