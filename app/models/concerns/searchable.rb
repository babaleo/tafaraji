module Searchable
  extend ActiveSupport::Concern

  included do
    # these include elasticsearch-rails modules
    include Elasticsearch::Model
    # include Elasticsearch::Model::Callbacks # enables auto-indexing

    # setup index configurations and mappings
    settings index: { 
      number_of_shards: 1, 
      number_of_replicas: 0,
      analysis: {
        filter: {
          my_shingle_filter: {
            type: "shingle",
            min_shingle_size: 2,
            max_shingle_size: 2,
            output_unigrams: false
          },
          my_stop_filter: {
            type: "stop",
            stopwords: "_english_"
          },
          nGram_filter: {
            type: "nGram",
            min_gram: 1,
            max_gram: 20,
            token_chars: [
              "letter",
              "digit",
              "punctuation",
              "symbol"
            ]
          }
        },
        analyzer: {
          my_title_shingle_analyzer: {
            type: "custom",
            tokenizer: "standard",
            filter: ["lowercase", "my_shingle_filter"]
          },
          my_standard_analyzer: {
            type: "standard",
            stopwords: "_english_"
          },
          nGram_analyzer: {
            type: "custom",
            tokenizer: "whitespace",
            filter: [
              "lowercase",
              "asciifolding",
              "nGram_filter"
            ]
          },
          whitespace_analyzer: {
            type: "custom",
            tokenizer: "whitespace",
            filter: [
              "lowercase",
              "asciifolding"
            ]
          }
        }
      }
    } do
      # mappings dynamic: 'false', _all: { index_analyzer: 'nGram_analyzer', search_analyzer: 'whitespace_analyzer' } do
      mappings dynamic: 'false' do
        indexes :title, type: 'multi_field' do 
          indexes :title, type: 'string', analyzer: 'my_standard_analyzer', include_in_all: true
          indexes :shingles, analyzer: 'my_title_shingle_analyzer', include_in_all: false
          indexes :autocomplete, type: 'string', index_analyzer: 'nGram_analyzer', search_analyzer: 'whitespace_analyzer', include_in_all: false
          indexes :english, analyzer: 'english', include_in_all: true
        end
        indexes :descriptiontext, type: 'multi_field' do
          indexes :descriptiontext, type: 'string', analyzer: 'my_standard_analyzer', include_in_all: true
          indexes :autocomplete, type: 'string', index_analyzer: 'nGram_analyzer', search_analyzer: 'whitespace_analyzer', include_in_all: false
        end
        indexes :description, type: 'multi_field' do
          indexes :description, type: 'string', analyzer: 'my_standard_analyzer', include_in_all: true
        end
        indexes :venue, type: 'multi_field' do
          indexes :venue, type: 'string', analyzer: 'my_standard_analyzer', include_in_all: true
          indexes :autocomplete, type: 'string', index_analyzer: 'nGram_analyzer', search_analyzer: 'whitespace_analyzer', include_in_all: false
        end
        indexes :event_category do
          indexes :name, type: 'multi_field' do
            indexes :name, type: 'string', analyzer: 'my_standard_analyzer', include_in_all: true
            indexes :raw, analyzer: 'keyword', include_in_all: false
            indexes :autocomplete, type: 'string', index_analyzer: 'nGram_analyzer', search_analyzer: 'whitespace_analyzer', include_in_all: false
            indexes :english, analyzer: 'english', include_in_all: true
          end
        end
        indexes :tickets do
          indexes :price, type: 'double', include_in_all: false
        end
        indexes :entry, analyzer: 'keyword', include_in_all: false
        indexes :is_active, analyzer: 'keyword', include_in_all: false
        indexes :point, type: "geo_point", include_in_all: false
        indexes :location do
          indexes :name, analyzer: 'keyword', include_in_all: false
        end
        # nested hidden documents of event_times allows me to order results based on the start dates that are gte to 'now'
        indexes :event_times, type: 'nested', include_in_parent: true do
          indexes :starts, type: 'date', include_in_all: false
          indexes :ends, type: 'date', include_in_all: false
          indexes :starts_string, type: 'multi_field' do
            indexes :starts_string, type: 'string', analyzer: 'my_standard_analyzer', include_in_all: false
            indexes :autocomplete, type: 'string', index_analyzer: 'nGram_analyzer', search_analyzer: 'whitespace_analyzer', include_in_all: false
          end
          indexes :starts_string_long, type: 'string', analyzer: 'my_standard_analyzer', include_in_all: false
        end
      end
    
    end

    # customizing the JSON serialization for Elasticsearch
    def as_indexed_json(options={})
      self.as_json(
        include: { event_category: { only: [:name] },
                   tickets: { only: [:price] },
                   event_times: { only: [:starts, :ends, :starts_string, :starts_string_long]},
                   location: { only: [:name] }
                 })
    end



    def self.search(query, options={})
      locations_list = Location.distinct.pluck(:name).map(&:downcase)
      event_categories_list = EventCategory.distinct.pluck(:name).sort
      entry_list = ["Free", "Paid"]

      # Prefill and set the query filters

      __set_filters = lambda do |key, f|
        @search_definition[:filter][:and] ||= []
        @search_definition[:filter][:and]  |= [f]

        @search_definition[:aggs][key.to_sym][:filter][:and] ||= []
        @search_definition[:aggs][key.to_sym][:filter][:and]  |= [f]
      end

      @search_definition = {
          query: {
            filtered: {
              query: {},
              filter: {}
            },
          },

          suggest: {},

          highlight: {
            pre_tags: ['<em>'],
            post_tags: ['</em>'],
            fields: {
              title: {},
              "title.english" => {},
              "title.shingles" => {},
              location: {},
              descriptiontext: { fragment_size: 35 },
              "event_category.name" => {},
              "event_category.name.english" => {}
            }
          },

          filter: {},

          aggs: {

            locations: {
              filter: {},
              aggs: {
                filtered_locations: {
                  filters: {
                    filters: {}
                  }
                }
              }
            },

            event_categories: {
              filter: {},
              aggs: {
                filtered_event_categories: {
                  filters: {
                    filters: {}
                  }
                } 
              }
            },

            dateranges: {
              filter: {},
              aggs: {
                filtered_range: {
                  date_range: {
                    field: "event_times.starts",
                    ranges:[
                      { key: "Today", from: "now", to: "now+3h+24h/d-3h" }, # the +3h/-3h takes care of the +03:00 timezone offset
                      { key: "Tomorrow", from: "now+3h+24h/d-3h", to: "now+3h+48h/d-3h" },
                      { key: "This Week", from: "now", to: "now+3h+1w/w-3h" },
                      { key: "This Weekend", from: "now+3h+1w/w-2d-3h", to: "now+3h+1w/w-3h" },
                      { key: "Next Week", from: "now+3h+1w/w-3h", to: "now+3h+2w/w-3h" },
                      { key: "This Month", from: "now", to: "now+3h+1M/M-3h" },
                      { key: "Next Month", from: "now+3h+1M/M-3h", to: "now+3h+2M/M-3h" }
                    ]
                  }
                }
              }
            },

            entry: {
              filter: {},
              aggs: {
                filtered_entry: {
                  filters: {
                    filters: {}
                  }
                }
              }
            }
            
          }
        }

      # generate filter hashes for locations, event_categories, entry and events_near_me
      @search_definition[:aggs][:locations][:aggs][:filtered_locations][:filters][:filters] = Hash[locations_list.map { |locality| [locality, { term: { "location.name": locality } } ] } ]
      @search_definition[:aggs][:event_categories][:aggs][:filtered_event_categories][:filters][:filters] = Hash[event_categories_list.map { |evt| [evt,{ term: { "event_category.name.raw" => evt }}] } ]
      @search_definition[:aggs][:entry][:aggs][:filtered_entry][:filters][:filters] = Hash[entry_list.map { |ent| [ent,{ term: { entry: ent }}] } ]                                                                                         

    unless query.blank?
        @search_definition[:query][:filtered][:query] = {
          bool: {
            must: [
              {
                multi_match: { # a multi-match query allows for one match query to be ran on multiple fields
                  query: query,
                  minimum_should_match: "30%",
                  fields: ["title^5", "title.english^4", "description^3", "location^2", "event_category.name", "event_category.name.english", "event_times.starts_string", "event_times.starts_string_long"]
                }
              },
              { term: { is_active: true } }
            ],
            should: {
              match: {
                'title.shingles' => query
              }
            }
          }
        }
        @search_definition[:suggest] = {
          text: query,
          phrase_suggest: {
            phrase: {
              analyzer: "standard",
              field: "_all",
              size: 1,
              max_errors: 2, # will correct a maximum of 2 mispelled words before returning the corrected phrase
              highlight: {
                pre_tag: '<em class="underline-bold">',
                post_tag: '</em>'
              },
              collate: {
                query: {
                  filtered: {
                    query: {
                      multi_match: {
                        query: "{{suggestion}}",
                        fields: ["title", "description", "location", "event_category.name"]
                      } 
                    },
                    filter: {
                      bool: {
                        must: [ 
                          { range: { "event_times.starts" => { gte: "now" } } }
                        ]
                      }
                    }
                  }
                }
              }
            }
          }
        }
      else

        @search_definition[:query][:filtered][:query] = {
          bool: {
            must: [
                { match_all: {} },
                { term: { is_active: true } }
            ]
          }
        }

        if options[:upcoming_ongoing] == 'ongoing'
          @search_definition[:sort] = { 
            "event_times.starts" => {
              order: "desc",
              nested_path: "event_times",
              nested_filter: {
                bool: {
                  must: [
                    { range: { "event_times.starts" => { lte: "now" } } },
                    { range: { "event_times.ends" => { gte: "now" } } }
                  ]
                }
              }
            } 
          }
        else
          @search_definition[:sort] = { 
            "event_times.starts" => {
              order: "asc",
              nested_path: "event_times",
              nested_filter: {
                range: {
                  "event_times.starts" => {
                    gte: "now"
                  }
                }
              }
            } 
          }
        end

      end

      if options[:upcoming_ongoing] == 'ongoing'
        @search_definition[:query][:filtered][:filter] = 
        {
          nested: {
            path: "event_times",
            filter: {
              bool: {
                must: [
                  { range: { "event_times.starts" => { lte: "now" } } },
                  { range: { "event_times.ends" => { gte: "now" } } }
                ]
              }
            }
          } 
        }
      else
        @search_definition[:query][:filtered][:filter] =
        {
          nested: {
            path: "event_times",
            filter: {
              bool: {
                must: [
                  { range: { "event_times.starts" => { gte: "now" } } }
                ]
              }
            }
          } 
        }
      end

      if options[:event_category_name]
        f = { term: { "event_category.name.raw" => options[:event_category_name] } }
        __set_filters.call(:dateranges, f)
        __set_filters.call(:entry, f)
        __set_filters.call(:locations, f)
      end

      if options[:from] && options[:to] && options[:date]
        f = { range: { "event_times.starts" => { gte: options[:from], lt: options[:to] } } }
        __set_filters.call(:event_categories, f)
        __set_filters.call(:entry, f)
        __set_filters.call(:locations, f)
      elsif options[:from] && options[:date]=="All Dates" && !options[:to]
        f = { range: { "event_times.starts" => { gte: options[:from] } } }
        __set_filters.call(:event_categories, f)
        __set_filters.call(:entry, f)
        __set_filters.call(:locations, f)
      end

      if options[:entry]
        f = { term: { entry: options[:entry] } }
        __set_filters.call(:dateranges, f)
        __set_filters.call(:event_categories, f)
        __set_filters.call(:locations, f)
      end

      if options[:location]
        f = { term: { "location.name": options[:location] } }
        __set_filters.call(:dateranges, f)
        __set_filters.call(:event_categories, f)
        __set_filters.call(:entry, f)
      end
      
      if options[:km] && options[:lat] && options[:lng]
        f = { geo_distance: { distance: options[:km] , point: "#{options[:lat]}, #{options[:lng]}" } }
        __set_filters.call(:dateranges, f)
        __set_filters.call(:event_categories, f)
        __set_filters.call(:entry, f)
        __set_filters.call(:locations, f)
      end

      __elasticsearch__.search(@search_definition)

    end

    def self.autocomplete(query, options={})

      __set_filters = lambda do |key, f|
        @autocomplete_definition[:filter][:and] ||= []
        @autocomplete_definition[:filter][:and]  |= [f]
      end

      @autocomplete_definition = {
        size: 8,
        query: {
          filtered: {
            query: {
              bool: {
                must: [
                  {
                    multi_match: {
                      query: query,
                      fields: ["title.autocomplete", "descriptiontext.autocomplete", "venue.autocomplete", "event_category.name.autocomplete", "event_times.starts_string.autocomplete"],
                      type: "cross_fields",
                      operator: "and"
                    }
                  },
                  { term: { is_active: true } }
                ]
              }
            },           
            filter: {}
          }
        },
        highlight: {
          fields: {
            'descriptiontext.autocomplete' => { fragment_size: 30, number_of_fragments: 1 }
          }
        },
        filter: {}
      }
      
      if options[:upcoming_ongoing] == 'ongoing'
        @autocomplete_definition[:query][:filtered][:filter] = 
        {
          nested: {
            path: "event_times",
            filter: {
              bool: {
                must: [
                  { range: { "event_times.starts" => { lte: "now" } } },
                  { range: { "event_times.ends" => { gte: "now" } } }
                ]
              }
            }
          } 
        }
      else
        @autocomplete_definition[:query][:filtered][:filter] =
        {
          nested: {
            path: "event_times",
            filter: {
              bool: {
                must: [
                  { range: { "event_times.starts" => { gte: "now" } } }
                ]
              }
            }
          } 
        }
      end

      if options[:event_category_name]
        f = { term: { "event_category.name.raw" => options[:event_category_name] } }
        __set_filters.call(:dateranges, f)
        __set_filters.call(:entry, f)
        __set_filters.call(:locations, f)
      end

      if options[:from] && options[:to] && options[:date]
        f = { range: { "event_times.starts" => { gte: options[:from], lt: options[:to] } } }
        __set_filters.call(:event_categories, f)
        __set_filters.call(:entry, f)
        __set_filters.call(:locations, f)
      elsif options[:from] && options[:date]=="All Dates" && !options[:to]
        f = { range: { "event_times.starts" => { gte: options[:from] } } }
        __set_filters.call(:event_categories, f)
        __set_filters.call(:entry, f)
        __set_filters.call(:locations, f)
      end

      if options[:entry]
        f = { term: { entry: options[:entry] } }
        __set_filters.call(:dateranges, f)
        __set_filters.call(:event_categories, f)
        __set_filters.call(:locations, f)
      end

      if options[:location]
        f = { term: { "location.name": options[:location] } }
        __set_filters.call(:dateranges, f)
        __set_filters.call(:event_categories, f)
        __set_filters.call(:entry, f)
      end
      
      if options[:km] && options[:lat] && options[:lng]
        f = { geo_distance: { distance: options[:km] , point: "#{options[:lat]}, #{options[:lng]}" } }
        __set_filters.call(:dateranges, f)
        __set_filters.call(:event_categories, f)
        __set_filters.call(:entry, f)
        __set_filters.call(:locations, f)
      end

      __elasticsearch__.search(@autocomplete_definition)
      
    end

  end
end