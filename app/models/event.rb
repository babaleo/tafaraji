class Event < ActiveRecord::Base
  is_impressionable
  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]
  
  before_save :set_descriptiontext
  include Searchable
  attachment :poster, type: :image
  validate :file_content_type?
  belongs_to :user
  belongs_to :event_category
  belongs_to :location
  has_many :saved_events, dependent: :destroy
  has_many :event_times, dependent: :destroy
  has_many :event_schedules, dependent: :destroy
  accepts_nested_attributes_for :event_schedules, :reject_if => :all_blank, :allow_destroy => true
  has_many :tickets, dependent: :destroy
  accepts_nested_attributes_for :tickets, :reject_if => :all_blank, :allow_destroy => true
  validates :title, uniqueness: { scope: :venue, conditions: -> { where("created_at > ?", 1.month.ago) }, message: " - Duplicate warning. An event with the same title and venue was created within the last month. Contact us if your event is different." }
  # validates :title, uniqueness: { scope: :venue }
  # validates :title, uniqueness: true
  validates :event_category_id, presence: true
  validates :location_id, presence: true
  validates :title, presence: true
  validates :title, length: { maximum: 80 }
  validates :event_schedules, presence: { message: "can't be blank, at least one event time is required" }
  validates :venue, presence: true
  validates :venue, length: { maximum: 200 }
  validates :entry, presence: true
  validates :poster, presence: true
  validates :description, presence: true, length: { minimum: 50 }
  validates :confirm_truth, acceptance: true
  validates :entry,
    :inclusion  => { :in => [ 'Free', 'Paid'],
    :message    => "%{value} is not a valid entry value" }
  before_validation { self.tickets.clear if self.entry == "Free" }
  # this reindexes the corresponding document in ElasticSearch when an event's event type changes
  after_update { __elasticsearch__.index_document if self.event_category_id_changed? }
  after_update { __elasticsearch__.index_document if self.location_id_changed? }

  after_commit on: [:create] do
    __elasticsearch__.index_document
  end

  after_commit on: [:update] do
    __elasticsearch__.update_document
  end

  after_commit on: [:destroy] do
    __elasticsearch__.delete_document
  end

  # takes care of instances where event is touched and document is created before a rollback happens
  # after_rollback { __elasticsearch__.try(:delete_document) }

  # this takes care of reindexing if ticket or time details change
  after_touch { __elasticsearch__.index_document }

  def set_descriptiontext
    self.descriptiontext = Nokogiri::HTML(self.description.gsub(/<p>|<ul>|<ol>|<li>/, ' ')).text.squish if self.description_changed?
  end

  def png?(data)
    pre_characters = data[0,4].force_encoding('UTF-8') 
    return pre_characters == "\x89PNG"
  end

  def gif?(data)
    pre_characters = data[0,4].force_encoding('UTF-8') 
    return pre_characters == "GIF8"
  end

  def jpeg?(data)
    pre_characters = data[0,2].force_encoding('UTF-8')
    return pre_characters == "\xFF\xD8"
  end

  def file_is_image?
    f = poster.to_io
    data = f.read(9) # magic numbers are up to 9 bytes
    f.close
    return png?(data) || gif?(data) || jpeg?(data)
  end

  def file_content_type?
    if self.poster.present? && !file_is_image?
      self.errors.add(:poster, "should be PNG, JPEG or GIF file type")
    end
  end

  # It returns the articles whose titles contain one or more words that form the query
  def self.shared_search(query)
    # where(:title, query) -> This would return an exact match of the query
    where("LOWER(title) like ?", "%#{query}%".downcase)
  end

  # Try building a slug based on the following fields in
  # increasing order of specificity.
  def slug_candidates
    [
      :title,
      [:title, :venue]
    ]
  end

end