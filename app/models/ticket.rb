class Ticket < ActiveRecord::Base
  belongs_to :event, touch: true

 	validates_numericality_of :price
 	validates :name, length: { maximum: 50 }
 	validates :price, presence: { message: "can't be blank if ticket name exists" }, if: :name
 	validates :name, presence: { message: "can't be blank if ticket price exists" }, if: :price
end