class ReportEvent < MailForm::Base

  attribute :name, :validate => true
  attribute :your_email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  # attribute :recipient_email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :subject, :validate => true
  attribute :message, :validate => true
  attribute :nickname, :captcha  => true

  # Declare the e-mail headers. It accepts anything the mail method in ActionMailer accepts.
  def headers
    {
      :subject => "#{subject}",
      :to => "admins@funjaza.co.ke",
      :from => %("#{name}" <#{your_email}>)
    }
  end
end