class ReportIssue < MailForm::Base
  include ActiveModel::Validations
  include ActiveModel::Validations::Callbacks
  before_validation :read_file

  append :remote_ip, :user_agent

  attribute :name, :validate => true
  attribute :email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :subject, :validate => true
  attribute :screenshot, :attachment => true
  # attribute :screenshot, :validate => :screenshot_attached?
  attribute :screenshot, :validate => :screenshot_size?
  attribute :screenshot, :validate => :file_content_type?
  attribute :message, :validate => true
  attribute :nickname, :captcha  => true

  def read_file
    unless screenshot.nil?
      @data = screenshot.read(9)
      screenshot.close()
      screenshot.open()
    end
  end

  # Declare the e-mail headers. It accepts anything the mail method in ActionMailer accepts.
  def headers
    {
      :subject => "#{subject}",
      :to => "support@funjaza.co.ke",
      :from => %("#{name}" <#{email}>)
    }
  end

  # def screenshot_attached?
  #   if screenshot.nil?
  #     self.errors.add(:screenshot, "can't be blank")
  #   end
  # end

  def screenshot_size?
    if screenshot.present? && screenshot.size > 2.megabytes
      self.errors.add(:screenshot, "should be less than 2MB")
    end
  end

  def png?(data)
    pre_characters = data[0,4].force_encoding('UTF-8') 
    return pre_characters == "\x89PNG" && screenshot.content_type == "image/png"
  end

  def gif?(data)
    pre_characters = data[0,4].force_encoding('UTF-8') 
    return pre_characters == "GIF8" && screenshot.content_type == "image/gif"
  end

  def jpeg?(data)
    pre_characters = data[0,2].force_encoding('UTF-8')
    return pre_characters == "\xFF\xD8" && screenshot.content_type == "image/jpeg"
  end

  def file_is_image?
    if @data
      return png?(@data) || gif?(@data) || jpeg?(@data)
    end
  end

  def file_content_type?
    if !file_is_image? && screenshot.present?
      self.errors.add(:screenshot, "should be PNG, JPEG or GIF file type")
    end
  end

end