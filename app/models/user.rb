class User < ActiveRecord::Base
  # extend FriendlyId
  # friendly_id :name, use: :slugged
  
  # will obfuscate event ids (upto 10 billion different possibilities)
  # NB: obfuscate_id messes up tests when looking up a fixture in test db as that process does not use find
  # and as a result does not deobfuscate
  # unless Rails.env.test?
  #   obfuscate_id spin: 34658291
  # end

  # Include default devise modules. Others available are:
  # :lockable and :timeoutable

  devise :database_authenticatable, :registerable,
   :recoverable, :rememberable, :trackable, :validatable, :confirmable, :omniauthable, :omniauth_providers => [:facebook]

  validates :name, presence: true, uniqueness: true
  validates :facebook_url, format: /\A((http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/, allow_blank: true
  validates :twitter_handle, format: /\A(\w){1,15}\z/, allow_blank: true

  has_many :events, dependent: :destroy
	has_many :saved_events, dependent: :destroy
  belongs_to :location

  # this method is called by devise to check for "active" state of the model i.e. whether a user is active or disabled.
  def active_for_authentication?
    super && self.is_active?
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name   # assuming the user model has a name
      user.skip_confirmation!
      user.save!
    end
  end
end