class EventTime < ActiveRecord::Base
  belongs_to :event, touch: true
  belongs_to :event_schedule
  before_save :set_starts_string

  private
    def set_starts_string
      self.starts_string = self.starts.to_datetime.strftime("%a, %d %b %Y, %l:%M%P")
      self.starts_string_long = self.starts.to_datetime.strftime("%A, %d %B %Y, %l:%M%P")
    end
end