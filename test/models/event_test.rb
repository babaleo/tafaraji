require 'test_helper'

class EventTest < ActiveSupport::TestCase

	def setup
		@user = users(:user_1)
		@event_category = event_categories(:concert)
		@event = @user.events.build(title: "Kumekucha", description: "We'll be rocking oldie but goodies!", 
                                starts_date: 1.hour.from_now, starts_time: 1.hour.from_now, 
                                ends_date: 10.hours.from_now, ends_time: 10.hours.from_now, event_category: @event_category,
                                location: "KICC", place_id: "GDehfjug", entry: "Free", point: "24,56")
	end

  test "event type should be present" do
    @event.event_category = nil
    assert_not @event.valid?, "Event without an event type is invalid."
  end

  test "title should be present" do
    @event.title = " "
    assert_not @event.valid?, "Event without a title is invalid."
  end

  test "description should be present" do
    @event.description = " "
    assert_not @event.valid?, "Event without a description is invalid."
  end

# split date time tests - these tests pass regardless of whether dates exist or not :(
  # test "starts date should be present" do
  #   @event.starts_date = nil
  #   assert_not @event.valid?, "Saved event without a starts date."
  # end

  # test "starts time should be present" do
  #   @event.starts_time = nil
  #   assert_not @event.valid?, "Saved event without a starts time."
  # end

  # test "ends date should be present" do
  #   @event.ends_date = nil
  #   assert_not @event.valid?, "Saved event without an ends date."
  # end

  # test "ends time should be present" do
  #   @event.ends_time = nil
  #   assert_not @event.valid?, "Saved event without an ends time."
  # end

  test "location information should be present" do
    @event.venue = " "
    assert_not @event.valid?, "Event without location information is invalid."
  end

  test "entry details should be present" do
    @event.entry = " "
    assert_not @event.valid?, "Event without entry details is invalid."
  end

  test "title should be a maximum length of 70 characters" do
    @event.title = 'a'*71
    assert_not @event.valid?, "Event title greater than 70 characters is invalid."
  end

end
