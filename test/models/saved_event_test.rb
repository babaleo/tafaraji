require 'test_helper'

class SavedEventTest < ActiveSupport::TestCase
	def setup
		@user = users(:user_1)
		@event = events(:event_1)
		@saved_event = @user.saved_events.build(event: @event)
	end

  test "saved event should have a unique combination of event and user" do
    assert_not @saved_event.valid?, "Saved event exists. Each saved event must have a unique combination of user and event."
  end
end
