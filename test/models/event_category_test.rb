require 'test_helper'

class EventCategoryTest < ActiveSupport::TestCase

  test "should not save event type without name" do
    event_category = EventCategory.new
    assert_not event_category.save, "Saved event type without name."
  end

end