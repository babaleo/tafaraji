require 'test_helper'

class EventsControllerTest < ActionController::TestCase
  setup do
    @event = events(:event_1)
    @event_category = event_categories(:concert)
    @user = users(:user_1)
    @other_user = users(:user_2)
  end

  test "should get index with upcoming events when locale & region are set" do
    get :index, locale: "en-KE" #what's the best way to deal with locale?
    assert_response :success
    assert_not_nil assigns(:events)
  end

  test "should get index but no events when default locale is set" do
    get :index, locale: I18n.default_locale
    assert_response :success
    assert_nil assigns(:events)
  end

  test "should redirect new to signin if no signed in user" do
    get :new, locale: "en-KE"
    assert_redirected_to user_session_url
  end

  test "should get new if user signed in" do
    sign_in @user
    get :new, locale: "en-KE"
    assert_response :success
  end

  test "should create event if user signed in" do
    sign_in @user
    assert_difference('Event.count') do
      # same as :event_1 in events fixture with the only difference being the title to ensure uniqueness
      post :create, locale: "en-KE", event: { description: @event.description, ends_date: @event.ends, ends_time: @event.ends, entry: @event.entry, event_category_id: @event_category.id, venue: @event.venue, starts_date: @event.starts, starts_time: @event.starts, title: "Hmm", user_id: @user.id }
    end
    assert_redirected_to event_path(assigns(:event))
  end

  test "should not create event if user signed in but event not unique" do
    sign_in @user
    assert_no_difference('Event.count') do
      post :create, locale: "en-KE", event: { description: @event.description, ends_date: @event.ends, ends_time: @event.ends, entry: @event.entry, event_category_id: @event_category.id, venue: @event.venue, starts_date: @event.starts, starts_time: @event.starts, title: @event.title, user_id: @user.id }
    end
    assert_select 'span.help-block', "This event has already been shared."
  end

  test "should show event" do
    get :show, locale: "en-KE", id: @event
    assert_response :success
  end

  test "should redirect edit to signin if no signed in user" do
    get :edit, locale: "en-KE", id: @event
    assert_redirected_to user_session_url
  end

  test "should get edit if event creator (user) is signed in" do
    sign_in @user
    get :edit, locale: "en-KE", id: @event
    assert_response :success
  end

  test "should redirect edit if wrong user (not creator) is signed in" do
    sign_in @other_user
    get :edit, locale: "en-KE", id: @event
    assert_redirected_to root_url
  end

  test "should redirect update if wrong user (not creator) is signed in" do
    sign_in @other_user
    patch :update, locale: "en-KE", id: @event, event: { description: @event.description, ends: @event.ends, entry: @event.entry, event_category_id: @event.event_category_id, venue: @event.venue, place_id: @event.place_id, point: @event.point, starts: @event.starts, title: @event.title, user_id: @event.user_id }
    assert_redirected_to root_url
  end

  test "should update event if event creator (user) is signed in" do
    sign_in @user
    patch :update, locale: "en-KE", id: @event, event: { description: @event.description, ends: @event.ends, entry: @event.entry, event_category_id: @event.event_category_id, venue: @event.venue, place_id: @event.place_id, point: @event.point, starts: @event.starts, title: @event.title, user_id: @event.user_id }
    assert_redirected_to event_path(assigns(:event))
  end

  test "should redirect update if no user is signed in" do
    patch :update, locale: "en-KE", id: @event, event: { description: @event.description, ends: @event.ends, entry: @event.entry, event_category_id: @event.event_category_id, venue: @event.venue, place_id: @event.place_id, point: @event.point, starts: @event.starts, title: @event.title, user_id: @event.user_id }
    assert_redirected_to user_session_url
  end

  test "should destroy event if event creator (user) is signed in" do
    sign_in @user
    assert_difference('Event.count', -1) do
      delete :destroy, id: @event, locale: "en-KE"
    end
    #perhaps I should redirect to users profile path
    assert_redirected_to events_path
  end

  test "should redirect destroy if wrong user (not creator) is signed in" do
    sign_in @other_user
    assert_no_difference('Event.count', -1) do
      delete :destroy, id: @event, locale: "en-KE"
    end
    assert_redirected_to root_url
  end

  test "should redirect destroy if no user is signed in" do
    assert_no_difference('Event.count', -1) do
      delete :destroy, id: @event, locale: "en-KE"
    end
    assert_redirected_to user_session_url
  end

end
