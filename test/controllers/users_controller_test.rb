require 'test_helper'

class UsersControllerTest < ActionController::TestCase
	def setup
		@user = users(:user_1)
		@other_user = users(:user_2)
	end

	test "should redirect show if logged in as wrong user" do
		sign_in @other_user
		get :show, id: @user, locale: "en-KE"
		assert flash.empty?
		assert_redirected_to root_url
	end
end