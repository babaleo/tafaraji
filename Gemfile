# MAKE SURE TO LOCK TO SPECIFIC GEM VERSION NUMBERS

source 'https://rubygems.org'
ruby "2.2.2"

gem 'rails', '4.2.4'
# Use postgresql as the database for Active Record
gem 'pg', '0.18.2'
# Will_paginate is a pagination library
gem 'will_paginate', '3.0.7'
# Use SCSS for stylesheets
gem 'sass-rails', '5.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '2.7.1'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '4.1.0'
# Use jquery as the JavaScript library
gem 'jquery-rails', '4.0.4'
# Provides access to jquery-ui
gem 'jquery-ui-rails', '5.0.5'
# displays progress bar when turbolinks/AJAX requests are fired
gem 'nprogress-rails', '0.1.6.7'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '2.3.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '0.4.1', group: :doc
# bootstrap-sass is a Sass-powered version of Bootstrap
gem 'bootstrap-sass', '3.3.5'
# Devise is a flexible authentication solution for Rails based on Warden. 
gem 'devise', '3.5.1'
# Rails forms made easy
gem 'simple_form', '3.1.0'
# These gems integrate ElasticSearch with Ruby-on-Rails
gem 'elasticsearch-model', '0.1.6'
gem 'elasticsearch-rails', '0.1.6'
# activeadmin provides an admin interface for the website
gem 'activeadmin', '1.0.0.pre1', github: 'activeadmin'
# date_time_attribute gem combines date and time field values
gem 'date_time_attribute', '0.1.2'
# bootstrap-datepicker-rails project integrates a datepicker with Rails 3 assets pipeline.
gem 'bootstrap-datepicker-rails', '1.4.0'
# recurring_select adds selectors and helpers for working with recurring schedules in a Rails app
gem 'recurring_select', '2.0.0'
# A simple date validator for Rails.
gem 'date_validator', '0.8.1'
# Chronic is a natural language date/time parser written in pure Ruby
gem 'chronic', '0.10.2'
# Cocoon makes it easier to handle nested forms. In this case, Tickets are nested within Events.
gem "cocoon", '1.2.6'
# refile gem is for event poster uploads
gem "refile", '0.6.1', require: "refile/rails"
gem "refile-mini_magick", "0.2.0"
# Adds font awesome
# gem 'font-awesome-sass', '4.3.2.1'
# Whenever is a Ruby gem that provides a clear syntax for writing and deploying cron jobs.
gem 'whenever', '0.9.4', :require => false
# Enables js based client validations in forms
gem 'client_side_validations', '4.2.0', github: 'DavyJonesLocker/client_side_validations'
# simple_form plugin for the client_side_validations gem 
gem 'client_side_validations-simple_form', '3.1.0', github: 'DavyJonesLocker/client_side_validations-simple_form'
# A great wysiwyg bootstrap-based editor 
gem 'summernote-rails', '0.6.7.0'
# Nokogiri is an HTML, XML, SAX, and Reader parser.
gem 'nokogiri', '1.6.6.2'
# applies bootstrap styling to will_paginate page links
gem 'will_paginate-bootstrap', '1.0.1'
# Mail_form allows you to Send e-mail straight from forms in Rails with I18n, validations, attachments and request information.
gem 'mail_form', '1.5.1'
# application server
gem 'puma', '2.12.3'
# NewRelic agent
gem 'newrelic_rpm', '3.13.0.299'
# SitemapGenerator is a framework-agnostic XML Sitemap generator written in Ruby with automatic Rails integration.
gem 'sitemap_generator', '5.1.0'
# FriendlyId allows you to create pretty URL’s and work with human-friendly strings as if they were numeric ids for ActiveRecord models
gem 'friendly_id', '~> 5.1.0'
# Search Engine Optimization (SEO) plugin for Ruby on Rails applications.
gem 'meta-tags', '2.0.0'

gem 'modernizr-rails', '2.7.1'

gem 'autoprefixer-rails', '6.0.3'

# gem 'sprockets-image_compressor', '0.3.0'

# gem "recurrence"

# A lightweight plugin that logs impressions per action or manually per model
gem 'impressionist'

# Simple, efficient background processing for Ruby.
# gem 'sidekiq'

# Facebook OAuth2 Strategy for OmniAuth
gem 'omniauth-facebook'

group :development do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
	# This is a small gem which causes rails console to open pry. It therefore depends on pry.
  gem 'pry-rails'
  # colorizes minitest output
  gem 'minitest-rg'
  # allows creation of fake objects in seeds file
  gem 'faker'
  # a complete geocoding solution for Ruby
  gem "geocoder"
  # consistency_fail is a tool to detect missing unique indexes in Rails projects.
  gem "consistency_fail"

  gem 'capistrano',         require: false
  gem 'capistrano-rbenv',   require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma',   require: false
end

group :production do
	# required by Heroku
	# gem 'rails_12factor'
	# bonsai-elasticsearch-rails’ gem is a bonsai-specific gem to get the client to work on Heroku
	# gem "bonsai-elasticsearch-rails"
  # For storing event posters on Amazon S3.
  # gem 'aws-sdk', '< 2.0'
  # allow refile to upload files to Amazon S3 easily
  gem 'refile-s3', '0.2.0'
  
end