class AddStartsStringToEventTimes < ActiveRecord::Migration
  def change
    add_column :event_times, :starts_string, :string
  end
end
