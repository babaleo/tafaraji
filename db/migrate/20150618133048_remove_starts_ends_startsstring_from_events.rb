class RemoveStartsEndsStartsstringFromEvents < ActiveRecord::Migration
  def change
  	remove_column :events, :starts, :datetime
  	remove_column :events, :ends, :datetime
  	remove_column :events, :startsstring, :string
  end
end