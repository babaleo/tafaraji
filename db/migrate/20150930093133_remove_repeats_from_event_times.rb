class RemoveRepeatsFromEventTimes < ActiveRecord::Migration
  def change
    remove_column :event_times, :repeats, :text
  end
end
