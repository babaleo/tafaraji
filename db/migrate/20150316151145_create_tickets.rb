class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :name
      t.decimal :price, precision: 8, scale: 2
      t.references :event, index: true

      t.timestamps null: false
    end
    add_foreign_key :tickets, :events
  end
end
