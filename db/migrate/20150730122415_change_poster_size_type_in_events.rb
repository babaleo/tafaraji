class ChangePosterSizeTypeInEvents < ActiveRecord::Migration
  def up
   change_column :events, :poster_size, 'integer USING CAST(poster_size AS integer)'
  end

  def down
   change_column :events, :poster_size, :string
  end
end
