class RemoveLocaleFromCountries < ActiveRecord::Migration
  def change
    remove_column :countries, :locale, :string
  end
end
