class AddIndexEventIdStartsToEventTimes < ActiveRecord::Migration
  def change
  	add_index :event_times, [:event_id, :starts], unique: true
  end
end