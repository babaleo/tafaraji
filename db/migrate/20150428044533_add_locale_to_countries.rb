class AddLocaleToCountries < ActiveRecord::Migration
  def change
    add_column :countries, :locale, :string
  end
end
