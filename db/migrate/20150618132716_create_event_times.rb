class CreateEventTimes < ActiveRecord::Migration
  def change
    create_table :event_times do |t|
      t.references :event, index: true
      t.datetime :starts
      t.datetime :ends
    end
    add_foreign_key :event_times, :events
    add_index :event_times, [:event_id, :starts, :ends], unique: true
  end
end
