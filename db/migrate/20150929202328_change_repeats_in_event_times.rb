class ChangeRepeatsInEventTimes < ActiveRecord::Migration
  def up
    change_column :event_times, :repeats, :text
  end

  def down
    change_column :event_times, :repeats, :boolean
  end
end
