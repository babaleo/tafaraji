class AddDescriptionTextToEvents < ActiveRecord::Migration
  def change
    add_column :events, :descriptiontext, :text
  end
end
