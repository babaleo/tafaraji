class RemovePlaceIdLocalityAdminAreasFromEvents < ActiveRecord::Migration
  def change
    remove_column :events, :place_id, :string
    remove_column :events, :locality, :string
    remove_column :events, :admin_area_1, :string
    remove_column :events, :admin_area_2, :string
  end
end
