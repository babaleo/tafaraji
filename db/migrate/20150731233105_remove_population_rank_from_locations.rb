class RemovePopulationRankFromLocations < ActiveRecord::Migration
  def change
    remove_column :locations, :population_rank, :integer
  end
end
