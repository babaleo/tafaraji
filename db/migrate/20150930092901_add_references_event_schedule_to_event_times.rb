class AddReferencesEventScheduleToEventTimes < ActiveRecord::Migration
  def change
    add_reference :event_times, :event_schedule, index: true, foreign_key: true
  end
end
