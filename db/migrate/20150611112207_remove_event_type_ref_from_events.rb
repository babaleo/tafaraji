class RemoveEventTypeRefFromEvents < ActiveRecord::Migration
  def change
  	remove_index :events, column: :event_type_id
  	remove_column :events, :event_type_id
  end
end
