class AddRepeatsToEventTimes < ActiveRecord::Migration
  def change
    add_column :event_times, :repeats, :boolean
  end
end
