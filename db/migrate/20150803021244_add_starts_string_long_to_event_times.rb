class AddStartsStringLongToEventTimes < ActiveRecord::Migration
  def change
    add_column :event_times, :starts_string_long, :string
  end
end
