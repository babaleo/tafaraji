class RemoveIndexEventIdStartsEndsFromEventTimes < ActiveRecord::Migration
  def change
  	remove_index :event_times, column: [:event_id, :starts, :ends]
  end
end