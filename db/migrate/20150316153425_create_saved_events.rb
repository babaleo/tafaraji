class CreateSavedEvents < ActiveRecord::Migration
  def change
    create_table :saved_events do |t|
      t.references :event, index: true
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :saved_events, :events
    add_foreign_key :saved_events, :users
  end
end
