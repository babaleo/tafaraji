class RenamePlaceToLocationInEvents < ActiveRecord::Migration
  def change
  	rename_column :events, :place, :location
  end
end
