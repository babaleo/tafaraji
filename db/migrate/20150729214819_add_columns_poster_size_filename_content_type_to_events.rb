class AddColumnsPosterSizeFilenameContentTypeToEvents < ActiveRecord::Migration
  def change
    add_column :events, :poster_size, :string
    add_column :events, :poster_filename, :string
    add_column :events, :poster_content_type, :string
  end
end
