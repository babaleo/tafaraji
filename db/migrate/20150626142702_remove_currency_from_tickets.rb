class RemoveCurrencyFromTickets < ActiveRecord::Migration
  def change
    remove_column :tickets, :currency, :string
  end
end
