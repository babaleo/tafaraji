class RemoveEndsFromEventTimes < ActiveRecord::Migration
  def change
    remove_column :event_times, :ends, :datetime
  end
end
