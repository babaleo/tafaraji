class AddIndexNameAndIndexPopulationRankToLocations < ActiveRecord::Migration
  def change
  	add_index :locations, :name, unique: true
  	add_index :locations, :population_rank, unique: true
  end
end
