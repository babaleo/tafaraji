class RenameLocationToVenueInEvents < ActiveRecord::Migration
  def change
  	rename_column :events, :location, :venue
  end
end
