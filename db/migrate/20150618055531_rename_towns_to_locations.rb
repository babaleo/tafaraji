class RenameTownsToLocations < ActiveRecord::Migration
  def change
  	rename_table :towns, :locations
  end
end