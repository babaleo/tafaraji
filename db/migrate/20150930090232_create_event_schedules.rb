class CreateEventSchedules < ActiveRecord::Migration
  def change
    create_table :event_schedules do |t|
      t.datetime :starts
      t.datetime :ends
      t.datetime :until
      t.text :recurrence_rule
      t.references :event, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
