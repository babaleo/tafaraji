class RenameRecurrenceRuleColumnToScheduleInEventSchedules < ActiveRecord::Migration
  def change
  	rename_column :event_schedules, :recurrence_rule, :schedule
  end
end
