class SetDefaultIsActiveEvents < ActiveRecord::Migration
  def change
  	change_column_default :events, :is_active, true
  	Event.update_all(:is_active => true)
  end
end