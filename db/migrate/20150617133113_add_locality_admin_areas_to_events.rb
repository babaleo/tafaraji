class AddLocalityAdminAreasToEvents < ActiveRecord::Migration
  def change
    add_column :events, :locality, :string
    add_column :events, :admin_area_1, :string
    add_column :events, :admin_area_2, :string
  end
end
