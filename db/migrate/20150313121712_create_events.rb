class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.datetime :starts
      t.datetime :ends
      t.references :user, index: true
      t.references :event_type, index: true
      t.text :schedule
      t.datetime :recurs_until
      t.string :place
      t.string :place_id
      t.string :entry
      t.string :point

      t.timestamps null: false
    end
    add_foreign_key :events, :users
    add_foreign_key :events, :event_types
  end
end