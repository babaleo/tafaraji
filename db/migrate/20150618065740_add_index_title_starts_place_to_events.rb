class AddIndexTitleStartsPlaceToEvents < ActiveRecord::Migration
  def change
  	add_index :events, [:title, :starts, :place], unique: true
  end
end
