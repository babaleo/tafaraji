class RenameEventTypesToEventCategories < ActiveRecord::Migration
  def change
  	rename_table :event_types, :event_categories
  end
end