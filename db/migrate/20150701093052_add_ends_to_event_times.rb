class AddEndsToEventTimes < ActiveRecord::Migration
  def change
    add_column :event_times, :ends, :datetime
  end
end
