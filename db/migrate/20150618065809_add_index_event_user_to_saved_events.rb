class AddIndexEventUserToSavedEvents < ActiveRecord::Migration
  def change
  	add_index :saved_events, [:event_id, :user_id], unique: true
  end
end
