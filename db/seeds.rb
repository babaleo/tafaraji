# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# countries
# locations
# event types
# users
# events
# tickets

# >> Steps to seed and update ES <<
# bundle exec rake db:migrate:reset
# bundle exec rake db:seed
# rails c
# Event.__elasticsearch__.create_index! force: true
# Event.__elasticsearch__.refresh_index!
# Event.import
# exit

# locations = Location.create([{ name: 'nairobi', population_rank: 1 }, 
# 										 { name: 'mombasa', population_rank: 2 }, 
# 										 { name: 'kisumu', population_rank: 3 }])

# event_categories = EventCategory.create([{ name: 'concert', description: 'concert' },
# 																{ name: 'sport', description: 'sport' },
# 																{ name: 'movie', description: 'movie' },
# 																{ name: 'nightlife', description: 'nightlife' },
# 																{ name: 'kiddy fun', description: 'kiddy fun' },
# 																{ name: 'play', description: 'play' }])

# places = [
# 					["nairobi","The Junction Mall, Nairobi, Kenya"],
# 					["mombasa","Whitesands Hotel, Mombasa, Kenya"],
# 					["kisumu","Imperial Hotel, Kisumu, Kenya"],
# 					["nairobi","IMAX 20th Century, Nairobi, Kenya"],
# 					["mombasa","Nyali Cinemax, Mombasa, Kenya"],
# 					["nairobi","KICC, Nairobi Kenya"],
# 					["kisumu","Moi Stadium, Kisumu, Kenya"],
# 					["kisumu","Kisumu Polytechnic, Kisumu, Kenya"],
# 					["nairobi","The Village Market, Nairobi, Kenya"],
# 					["mombasa","Tononoka Stadium, Mombasa, Kenya"]
# 				 ]

# 50.times do |n|
# 	name = Faker::Name.name
# 	email = "example-#{n+1}@gmail.com"
# 	password = "password"
# 	User.create!(email: email,
# 							 password: password,
# 							 password_confirmation: password,
# 							 confirmed_at: Time.zone.now)
# end

# entry_options = ['Free', 'Paid']
# users = User.order(:created_at).take(10)
# 50.times do
# 	title = Faker::Company.bs
# 	description = Faker::Company.catch_phrase
# 	starts = Faker::Time.between(Time.zone.now, 3.days.from_now)
# 	ends = Faker::Time.between(4.days.from_now, 7.days.from_now)
# 	event_category_id = event_categories.sample.id
# 	location_place = places.sample
# 	location = location_place[0]
# 	place = location_place[1]
# 	geocoding_result = Geocoder.search(place)
# 	lat = geocoding_result[0].data["geometry"]["location"]["lat"]
# 	lng = geocoding_result[0].data["geometry"]["location"]["lng"]
# 	entry = entry_options.sample
# 	user = users[rand(0..9)]
# 	user.events.create!(title: title,
# 											description: description,
# 										  starts: starts,
# 										  ends: ends,
# 										  event_category_id: event_category_id,
# 										  place: place,
# 										  latitude: lat,
# 										  longitude: lng,
# 										  locality: location,
# 										  admin_area_1: location,
# 										  admin_area_2: location,
# 										  entry: entry)
# end

# Event.all.each { |event| event.poster = File.open(Dir.glob(File.join(Rails.root, 'sample_images', '*')).sample); event.save! }


#################################################################

#Populate Location table

Location.delete_all

["nairobi",
 "mombasa",
 "kisumu",
 "kwale",
 "kilifi",
 "lamu",
 "taita taveta",
 "garissa",
 "wajir",
 "mandera",
 "nyandarua",
 "nyeri",
 "kirinyaga",
 "muranga",
 "kiambu",
 "marsabit",
 "isiolo",
 "meru",
 "tharaka",
 "embu",
 "kitui",
 "machakos",
 "makueni",
 "turkana",
 "west pokot",
 "samburu",
 "uasin gishu",
 "elgeyo marakwet",
 "nandi",
 "baringo",
 "laikipia",
 "nakuru",
 "narok",
 "kajiado",
 "bomet",
 "kericho",
 "kakamega",
 "vihiga",
 "bungoma",
 "busia",
 "siaya",
 "homa bay",
 "migori",
 "kisii",
 "nyamira",
 "trans nzoia",
 "tana river"].each do |county|
	Location.where(name: county).first_or_create
end


#Populate Event Category table

EventCategory.delete_all

["Art Exhibition", "Film", "Food & Drink", "Kids Event", "Music", "Nightlife", "Other", "Sport", "Theatre & Dance"].each do |category|
	EventCategory.where(name: category).first_or_create(description: category)
end

###############################################################################